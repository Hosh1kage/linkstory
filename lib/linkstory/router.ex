# TODO: Add @spec to all functions for aditional validation;
defmodule Linkstory.Router do
  @moduledoc false
  use Plug.Router
  use Plug.Debugger
  use Plug.ErrorHandler
  alias Linkstory.Util.LogFormatter
  alias Linkstory.Util.DomainExtractor
  require Logger

  plug Plug.Logger, log: :debug
  plug Linkstory.Plug.Timestamper
  plug Linkstory.Plug.Identifier
  plug :match
  plug :dispatch

  @domain_storage Application.fetch_env!(:linkstory, :domain_storage)[:impl]

  @on_load :on_load
  def on_load() do
    Logger.info("Module [#{__MODULE__}] is loading;")
    # NOTE: Ad Hoc to trigger module @on_load method;
    @domain_storage.module_info()
    :ok
  end

  ######################################################################
  # PING
  ######################################################################

  get "/ping.text" do
    conn
    |> put_resp_content_type("text/plain", "utf-8")
    |> send_resp(200, "pong")
  end

  get "/ping.json" do
    conn
    |> put_resp_content_type("application/json", "utf-8")
    |> send_resp(200, Poison.encode!(%{ping: "pong"}))
  end

  ######################################################################
  # (C)RUD
  ######################################################################

  # TODO: Find a better way to handle errors;
  post "/visited_links" do
    conn = try do
      case conn |> read_body() do
        {:ok, body, conn} ->
          handle_visited_links!(body, conn.private.req_timestamp)
          conn |> put_resp_json!(200, "ok")
        _any ->
          conn |> put_resp_json!(400, "bad_request", "BRE001", conn.private.req_id)
      end
    rescue
      e1 in [ArgumentError] ->
        Logger.error(LogFormatter.format_error(e1, conn.private.req_id))
        conn |> put_resp_json!(500, "internal_server_error", "ISE003", conn.private.req_id)
      e2 ->
        Logger.error(LogFormatter.format_error(e2, conn.private.req_id))
        conn |> put_resp_json!(500, "internal_server_error", "ISE002", conn.private.req_id)
    end
    conn |> send_resp()
  end

  defp handle_visited_links!(body, timestamp) do
    handle_visited_links_validate!(body, timestamp)
    links = body
    # TODO: Decode only relevant data instead of full body;
    |> Poison.decode!()
    |> Map.get("links")
    unless is_nil(links) do
      # TODO: Add sanitize and normilize steps;
      links
      |> Enum.map(fn (link) -> extract_domain!(link) end)
      |> Enum.uniq()
      |> Enum.filter(fn (domain) -> not is_nil(domain) end)
      # NOTE: Save at least 1 link to avoid storage overloading with useless data;
      |> (fn (domains) -> unless domains == [], do: @domain_storage.save_domains!(domains, timestamp) end).()
    end
  end

  defp handle_visited_links_validate!(body, timestamp) do
    if is_nil(body) or is_nil(timestamp) do
      raise(ArgumentError, "Body and timestamp can't be nil;")
    end
  end

  defp extract_domain!(link) do
    extract_domain_validate!(link)
    DomainExtractor.extract(link)
  end

  defp extract_domain_validate!(link) do
    if is_nil(link) do
      raise(ArgumentError, "Link can't be nil;")
    end
  end

  ######################################################################
  # C(R)UD
  ######################################################################

  get "/visited_domains" do
    conn = try do
      conn = conn |> fetch_query_params()
      conn |> put_resp_json!(200, "ok", %{domains: handle_visited_domains!(conn.query_params["from"], conn.query_params["to"])})
    rescue
      e1 in [ArgumentError] ->
        Logger.error(LogFormatter.format_error(e1, conn.private.req_id))
        conn |> put_resp_json!(500, "internal_server_error", "ISE003", conn.private.req_id)
      e2 ->
        Logger.error(LogFormatter.format_error(e2, conn.private.req_id))
        conn |> put_resp_json!(500, "internal_server_error", "ISE002", conn.private.req_id)
    end
    conn |> send_resp()
  end

  defp handle_visited_domains!(from, to) do
    handle_visited_domains_validate!(from, to)
    @domain_storage.read_domains!(from, to)
    |> Enum.uniq()
    |> Enum.sort()
  end

  defp handle_visited_domains_validate!(from, to) do
    if is_nil(from) or is_nil(to) do
      raise(ArgumentError, "Search range boundaries can't be nil;")
    end
    unless String.match?(from, ~r/\d{10}/) and String.match?(to, ~r/\d{10}/) do
      raise(ArgumentError, "Search range boundaries [#{from}, #{to}] must be valid ('from' and 'to' must contain 10 digits only);")
    end
    unless String.to_integer(from) <= String.to_integer(to) do
      raise(ArgumentError, "Search range boundaries [#{from}, #{to}] must be valid ('from' must be less than or equal to 'to');")
    end
  end

  ######################################################################
  # DEAFAULT
  ######################################################################

  match _ do
    Logger.warn("[#{conn.private.req_id}] Resource not found;")
    conn
    |> put_resp_json!(404, "not_found", "NFE001", conn.private.req_id)
    |> send_resp()
  end

  ######################################################################
  # ERRORS
  ######################################################################

  def handle_errors(conn, %{kind: kind, reason: reason, stack: stack}) do
    Logger.error("[#{conn.private.req_id}] [ISE003] #{kind} - #{reason}; \n #{stack}")
    conn
    |> put_resp_json!(500, "internal_server_error", "ISE001", conn.private.req_id)
    |> send_resp()
  end

  ######################################################################
  # MISC
  ######################################################################

  defp put_resp_json!(conn, status_code, status_decs, data \\ nil) do
    put_resp_json!(conn, status_code, status_decs, nil, nil, data)
  end

  # TODO: Add parameter with error description and some recomendations how to fix it;
  # TODO: Make 'error_code' and 'error_ref' required if one of them is exist;
  defp put_resp_json!(conn, status_code, status_decs, error_code, error_ref, data \\ nil) do
    put_resp_json_validate!(conn, status_code, status_decs, error_code, error_ref, data)
    result = %{status: status_decs}
    result = unless is_nil(error_code), do: result |> Map.put(:error_code, error_code), else: result
    result = unless is_nil(error_ref), do: result |> Map.put(:error_ref, error_ref), else: result
    # NOTE: Use merge/3 function instead of merge/2 to avoid overriding standart keys like 'status', etc;
    result = unless is_nil(data), do: result |> Map.merge(data, fn (_k, v1, _v2) -> v1 end), else: result
    result = result |> Poison.encode!()
    conn
    |> put_resp_content_type("application/json", "utf-8")
    |> resp(status_code, result)
  end

  defp put_resp_json_validate!(conn, status_code, status_decs, error_code, error_ref, data \\ nil) do
    if is_nil(conn) do
      raise(ArgumentError, "Connection can't be nil;")
    end
    if is_nil(status_code) or is_nil(status_decs) do
      raise(ArgumentError, "Status code and description can't be nil;")
    end
  end

  # NOTE: Might be usefull in case if 'conn' or 'conn.private.req_id' is undefined;
  defp get_default_uuid() do
    "00000000-0000-0000-0000-000000000000"
  end

end
