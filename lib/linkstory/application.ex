defmodule Linkstory.Application do
  @moduledoc false
  use Application

  def start(_type, _args) do
    children = [
      Plug.Adapters.Cowboy.child_spec(
        scheme: :http,
        plug: Linkstory.Router,
        # TODO: Add default values if :server key is exist but without options;
        options: [port: Application.get_env(:linkstory, :server, [port: 5000])[:port]]
      )
    ]
    opts = [
      name: Linkstory.Supervisor,
      strategy: :one_for_one
    ]
    Supervisor.start_link(children, opts)
  end

end
