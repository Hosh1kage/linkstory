defmodule Linkstory.Plug.Timestamper do
	@moduledoc false
	@behaviour Plug
	alias Plug.Conn

	@date_time Application.fetch_env!(:linkstory, :date_time)[:impl]

	@impl true
  def init(opts) do
    opts
  end

	@impl true
  def call(conn, _opts) do
    conn |> Conn.put_private(:req_timestamp, @date_time.now_unix())
  end

end
