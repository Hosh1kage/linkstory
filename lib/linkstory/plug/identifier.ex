# NOTE: This is alternative for standart 'Plug.RequestId' module.
# This module is slightly better because it doesn't depend on
# request header 'x-request-id' which might be used in a wrong way.
# For example, if client will send same id in the header over and over again
# then some application logic will be mixed;
defmodule Linkstory.Plug.Identifier do
	@moduledoc false
	@behaviour Plug
	alias Plug.Conn

	@id_generator Application.fetch_env!(:linkstory, :id_generator)[:impl]

	@impl true
  def init(opts) do
    opts
  end

	@impl true
  def call(conn, _opts) do
    conn |> Conn.put_private(:req_id, @id_generator.generate_uuid())
  end

end
