defmodule Linkstory.Util.DateTime do
  @moduledoc false

  @callback now_unix() :: String.t()
  def now_unix() do
    DateTime.utc_now()
    |> DateTime.to_unix()
    |> to_string()
  end

end
