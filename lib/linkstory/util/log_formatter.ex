defmodule Linkstory.Util.LogFormatter do
  @moduledoc false

  def format_error(error) do
    "#{inspect(error)}:\n\n#{Exception.format_stacktrace()}"
  end

  def format_error(error, error_ref) do
    "[#{error_ref}] #{inspect(error)}:\n\n#{Exception.format_stacktrace()}"
  end

end
