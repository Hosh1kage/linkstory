defmodule Linkstory.Util.DomainExtractor do
  @moduledoc false

  def extract(link) do
    # TODO: Optimize regex and add next cases: ftp, localhost, ip, port, several dots/dashes in a row, etc;
    matches = Regex.run(~r/^(?:https?:\/\/)?+(?:www\d*\.)?+(?:[^@\n]+@)?+([\p{L}\d\.-]+\.[\p{L}\d-]+)/u, link, [capture: :all_but_first])
    unless is_nil(matches), do: hd(matches), else: nil 
  end

end
