defmodule Linkstory.Util.IdGenerator do
  @moduledoc false

  @callback generate_uuid() :: String.t()
  def generate_uuid() do
    UUID.uuid4()
  end

end
