defmodule Linkstory.Data.DomainStorageRedis do
  @moduledoc false
  @behaviour Linkstory.Data.DomainStorageBehaviour
  alias Linkstory.Data.DomainStorageRedisConnPool, as: RedisConnPool
  require Logger

  @on_load :on_load
  def on_load() do
    Logger.info("Module [#{__MODULE__}] is loading;")
    # NOTE: Ad Hoc to trigger module @on_load method;
    RedisConnPool.module_info()
    :ok
  end

  @impl true
  def save_domains!(domains, timestamp) do
    save_domains_validate!(domains, timestamp)
    RedisConnPool.get_write_conn() |> Redix.transaction_pipeline!([
      ["SADD", "domains:#{timestamp}"] ++ domains,
      ["ZADD", "domains:story", timestamp, timestamp]
    ])
    :ok
  end

  defp save_domains_validate!(domains, timestamp) do
    if is_nil(domains) or is_nil(timestamp) do
      raise(ArgumentError, "Domains list and timestamp can't be nil;")
    end
  end

  @impl true
  def read_domains!(from, to) do
    read_domains_validate!(from, to)
    conn = RedisConnPool.get_read_conn()
    links_sets = conn
      |> Redix.command!(["ZRANGEBYSCORE", "domains:story", from, to])
      |> Enum.map(fn (timestamp) -> "domains:#{timestamp}" end)
    # NOTE: Invoke 'SUNION' only if we found at least 1 set to union;
    if links_sets == [], do: [], else: conn |> Redix.command!(["SUNION" | links_sets])
  end

  defp read_domains_validate!(from, to) do
    if is_nil(from) or is_nil(to) do
      raise(ArgumentError, "Search range boundaries can't be nil;")
    end
  end

  @impl true
  def read_domains_all!() do
    conn = RedisConnPool.get_read_conn()
    links_sets = conn
      |> Redix.command!(["ZRANGE", "domains:story", 0, -1])
      |> Enum.map(fn (timestamp) -> "domains:#{timestamp}" end)
    # NOTE: Invoke 'SUNION' only if we found at least 1 set to avoid exception;
    if links_sets == [], do: [], else: conn |> Redix.command!(["SUNION" | links_sets])
  end

  @impl true
  def delete_domains!(from, to) do
    delete_domains_validate!(from, to)
    conn = RedisConnPool.get_write_conn()
    # TODO: Implement;
    :ok
  end

  defp delete_domains_validate!(from, to) do
    if is_nil(from) or is_nil(to) do
      raise(ArgumentError, "Delete range boundaries can't be nil;")
    end
  end

  @impl true
  def delete_domains_all!() do
    conn = RedisConnPool.get_write_conn()
    # TODO: Change implementation to smth less "destructive";
    conn |> Redix.command!(["FLUSHALL"])
    :ok
  end

end
