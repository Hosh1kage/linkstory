defmodule Linkstory.Data.DomainStorageRedisConnPool do
  @moduledoc false
  require Logger

  @ds_host Application.fetch_env!(:linkstory, :domain_storage)[:host]
  @ds_port Application.fetch_env!(:linkstory, :domain_storage)[:port]

  # TODO: Use config to set up conn pool;
  @ds_read_conn_size 1
  @ds_write_conn_size 5

  @on_load :on_load
  def on_load() do
    Logger.info("Module [#{__MODULE__}] is loading;")
    Process.spawn(fn () ->
      # TODO: Use 'Supervisor' to protect ETS process
      # or use smth like 'cachex' or 'poolboy' instead of simple ETS;
      :ets.new(:ds_read_write_pool, [:named_table, :set, :protected])
      # NOTE: Both read and write conns from the pool have number 1 as start index;
      Enum.each(1..@ds_read_conn_size, fn (id) -> add_pool_conn(:read, id) end)
      Enum.each(1..@ds_write_conn_size, fn (id) -> add_pool_conn(:write, id) end)
      Process.sleep(:infinity)
    end, [])
    :ok
  end

  def get_read_conn() do
    get_conn(:read)
  end

  def get_write_conn() do
    get_conn(:write)
  end

  defp get_conn(type) do
    case type do
      :read ->
        [{_key, value}] = :ets.lookup(:ds_read_write_pool, "read_conn_#{:rand.uniform(@ds_read_conn_size)}")
        value
      :write ->
        [{_key, value}] = :ets.lookup(:ds_read_write_pool, "write_conn_#{:rand.uniform(@ds_write_conn_size)}")
        value
      _any ->
        :error
    end
  end

  defp add_pool_conn(type, id) do
    {:ok, conn} = Redix.start_link(host: @ds_host, port: @ds_port)
    case type do
      :read ->
        :ets.insert(:ds_read_write_pool, {"read_conn_#{id}", conn})
      :write ->
        :ets.insert(:ds_read_write_pool, {"write_conn_#{id}", conn})
      _any ->
        :error
    end
  end

end
