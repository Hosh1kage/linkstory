# TODO: Change return types to smth like 'tuple()' with {:ok, ...}, {:error, ...} variants;
defmodule Linkstory.Data.DomainStorageBehaviour do
  @moduledoc false
  @callback save_domains!(domains :: [String.t()], timestamp :: String.t()) :: atom()
  @callback read_domains!(from :: String.t(), to :: String.t()) :: [String.t()]
  @callback read_domains_all!() :: [String.t()]
  @callback delete_domains!(from :: String.t(), to :: String.t()) :: atom()
  @callback delete_domains_all!() :: atom()
end
