defmodule Linkstory.MixProject do
  use Mix.Project

  @test_envs [:test, :test_unit, :test_integration]

  def project() do
    [
      app: :linkstory,
      version: "0.9.1",
      elixir: "~> 1.9",
      aliases: aliases(),
      preferred_cli_env: preferred_cli_env(),
      start_permanent: start_permanent(),
      elixirc_paths: elixirc_paths(),
      deps: deps()
    ]
  end

  def application() do
    [
      extra_applications: [
        :logger,
        :plug_cowboy
      ],
      mod: {Linkstory.Application, []}
    ]
  end

  # TODO: Change mix test command behaviour to smth like "runs all tests and pays respect to CLI arguments";

  defp aliases() do
    [
      # NOTE: Default mix test command is disabled because of "test.integration" allias unexpected behaviuor;
      "test.unit": fn (_args) ->
        System.put_env("MIX_ENV", "test_unit")
        print_banner("Application unit tests will be started with [#{inspect(Mix.env())}] environment;")
        Mix.Task.run("test", ["--only", "unit_test:true", "--color", "--trace"])
      end,
      "test.integration": fn (_args) ->
        System.put_env("MIX_ENV", "test_integration")
        print_banner("Application integration tests will be started with [#{inspect(Mix.env())}] environment;")
        Mix.Task.run("test", ["--only", "integration_test:true", "--color", "--trace"])
      end,
      "run.dev": fn (_args) ->
        System.put_env("MIX_ENV", "dev")
        print_banner("Application will be started with [#{inspect(Mix.env())}] environment;")
        Mix.Task.run("run", ["--no-halt"])
      end,
      "run.prod": fn (_args) ->
        System.put_env("MIX_ENV", "prod")
        print_banner("Application will be started with [#{inspect(Mix.env())}] environment;")
        Mix.Task.run("run", ["--no-halt"])
      end
    ]
  end

  defp preferred_cli_env() do
    [
      # NOTE: Default mix test command is disabled because of "test.integration" allias unexpected behaviuor;
      "test.unit": :test_unit,
      "test.integration": :test_integration,
      "run.dev": :dev,
      "run.prod": :prod
    ]
  end

  defp start_permanent() do
    case Mix.env() do
      :prod ->
        true
      _any ->
        false
    end
  end

  defp elixirc_paths() do
    if Mix.env() in @test_envs do
      ["lib", "test/mock"]
    else
      ["lib"]
    end
  end

  defp deps() do
    [
			{:plug_cowboy, "~> 2.1"},
			{:poison, "~> 4.0"},
			{:redix, "~> 0.10"},
      {:uuid, "~> 1.1"},
      {:mox, "~> 0.5", only: @test_envs}
    ]
  end

  # TODO: Use 'Mix.shell().info("...")' instead?;
  defp print_banner(title) do
    IO.puts("\n****************************************************************************************************")
    IO.puts("* ")
    IO.puts("* #{title}")
    IO.puts("* ")
    IO.puts("****************************************************************************************************\n")
  end

end
