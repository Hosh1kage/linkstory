Mox.defmock(Linkstory.Util.DateTimeMock, for: Linkstory.Util.DateTime)
Mox.defmock(Linkstory.Util.IdGeneratorMock, for: Linkstory.Util.IdGenerator)
Mox.defmock(Linkstory.Data.DomainStorageMock, for: Linkstory.Data.DomainStorageBehaviour)
