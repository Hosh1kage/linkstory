defmodule Linkstory.Plug.TimestamperTest do
  use ExUnit.Case
  use Plug.Test
  alias Linkstory.Plug.Timestamper
  alias Linkstory.Util.DateTimeMock
  import Mox

  # TODO: Add repeatable test to check for timestamp dynamic and timeline correctness;
  describe "'Linkstory.Plug.Timestamper'" do

    @tag :unit_test
    test "(U) sets up request timestamp into 'conn.privates.req_timestamp' field" do
      DateTimeMock |> expect(:now_unix, fn () -> "1572545794" end)
      # TODO: Add more 'conn' variations;
      conn = conn("get", "/")
      refute(conn.private |> Map.has_key?(:req_timestamp))
      conn = Timestamper.call(conn, nil)
      assert(conn.private |> Map.has_key?(:req_timestamp))
      assert(conn.private.req_timestamp, "1572545794")
    end

    @tag :integration_test
    test "(I) sets up request timestamp into 'conn.privates.req_timestamp' field" do
      # TODO: Add more 'conn' variations;
      conn = conn("get", "/")
      refute(conn.private |> Map.has_key?(:req_timestamp))
      conn = Timestamper.call(conn, nil)
      assert(conn.private |> Map.has_key?(:req_timestamp))
      # NOTE: Since small but acceptable delay between setting 'conn' timestamp
      # and getting new one for the test is possible we have to keep it in mind;
      assert_in_delta(String.to_integer(conn.private.req_timestamp), DateTime.utc_now() |> DateTime.to_unix(), 2)
    end

  end

end
