defmodule Linkstory.Plug.IdentifierTest do
  use ExUnit.Case
  use Plug.Test
  alias Linkstory.Plug.Identifier
  alias Linkstory.Util.IdGeneratorMock
  import Mox

  # TODO: Add repeatable integration test to check for identifier randomness;
  describe "'Linkstory.Plug.Identifier'" do

    @tag :unit_test
    test "(U) sets up request identifier into 'conn.privates.req_timestamp' field" do
      IdGeneratorMock |> expect(:generate_uuid, fn () -> "24dd02dc-ebcf-437a-8d08-488168d69328" end)
      # TODO: Add more 'conn' variations;
      conn = conn("get", "/")
      refute(conn.private |> Map.has_key?(:req_id))
      conn = Identifier.call(conn, nil)
      assert(conn.private |> Map.has_key?(:req_id))
      assert(conn.private.req_id == "24dd02dc-ebcf-437a-8d08-488168d69328")
    end

    @tag :integration_test
    test "(I) sets up request identifier into 'conn.privates.req_timestamp' field" do
      # TODO: Add more 'conn' variations;
      conn = conn("get", "/")
      refute(conn.private |> Map.has_key?(:req_id))
      conn = Identifier.call(conn, nil)
      assert(conn.private |> Map.has_key?(:req_id))
      assert(conn.private.req_id |> String.match?(~r/^[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{12}$/))
    end

  end

end
