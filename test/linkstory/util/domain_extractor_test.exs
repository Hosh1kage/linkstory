defmodule Linkstory.Util.DomainExtractorTest do
  use ExUnit.Case
  alias Linkstory.Util.DomainExtractor

  describe "'Linkstory.Util.DomainExtractor.extract/1'" do

    # TODO: Add more links variations;
    @tag :unit_test
    test "(U) extracts domain part from the most of the links" do
      test_cases = [
        {"http://google", nil},
        {"https://google", nil},
        {"http://гугл", nil},
        {"https://гугл", nil},
        {"http://google/com", nil},
        {"https://google/com", nil},
        {"http://гугл/ру", nil},
        {"https://гугл/ру", nil},
        {"http://google.com", "google.com"},
        {"http://goo-gle.com", "goo-gle.com"},
        {"http://www.google.com", "google.com"},
        {"http://www.goo-gle.com", "goo-gle.com"},
        {"http://app.google.com", "app.google.com"},
        {"http://гугл.ру", "гугл.ру"},
        {"http://гу-гл.ру", "гу-гл.ру"},
        {"http://www.гугл.ру", "гугл.ру"},
        {"http://www.гу-гл.ру", "гу-гл.ру"},
        {"http://www.апп.гугл.ру", "апп.гугл.ру"},
        {"http://www.google.com?ref=ya.ru", "google.com"},
        {"http://www.google.com/search/news?ref=ya.ru", "google.com"},
        {"http://mailto:dev@google.com", "google.com"},
        {"http://user:pass@google.com?ref=ya.ru", "google.com"},
        {"ttp://www.google.com?ref=ya.ru", nil},
        {"://www.google.com?ref=ya.ru", nil},
        {"//www.google.com?ref=ya.ru", nil},
        {"/www.google.com?ref=ya.ru", nil},
        {"www.google?ref=ya.ru", nil},
        {"google.com?ref=ya.ru", "google.com"},
        {"ogle.com?ref=ya.ru", "ogle.com"},
        {"e.com?ref=ya.ru", "e.com"},
        {".com?ref=ya.ru", nil},
        {"www0.google.com?ref=ya.ru", "google.com"},
        {"www5.google.com?ref=ya.ru", "google.com"},
        {"www9.google.com?ref=ya.ru", "google.com"},
        {"www19.google.com?ref=ya.ru", "google.com"},
        {"www.g00gle.com?ref=ya.ru", "g00gle.com"},
        {"ref=ya.ru", nil}
      ]
      test_cases |> Enum.each(fn ({link, domain}) -> assert(DomainExtractor.extract(link) == domain) end)
    end

  end

end
