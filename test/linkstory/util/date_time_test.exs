# TODO: Add test for timestamps differences;
# TODO: Add integration test using 3rd part resources (smth like "http://worldclockapi.com/");
defmodule Linkstory.Util.DateTimeTest do
  use ExUnit.Case
  alias Linkstory.Util.DateTime

  describe "'Linkstory.Util.DateTime.now_unix/0'" do

    # TODO: Add more deterministic unit test instead of this one;
    @tag :unit_test
    test "(U) gives valid and unrepeatable UTC timestamp in Unix format (3 times in a row)" do
      ids = (0..2)
        |> Enum.map(fn (i) ->
          Process.sleep(1100)
          DateTime.now_unix()
        end)
        |> Enum.uniq()
      assert(length(ids) == 3)
      ids |> Enum.each(fn (timestamp) -> assert(timestamp |> String.match?(~r/^\d{10}$/)) end)
    end

  end

end
