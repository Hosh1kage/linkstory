defmodule Linkstory.Util.IdGeneratorTest do
  use ExUnit.Case
  alias Linkstory.Util.IdGenerator

  describe "'Linkstory.Util.IdGenerator.generate_uuid/0'" do

    # TODO: Add more deterministic unit test instead of this one;
    @tag :unit_test
    test "(U) generates valid and unrepeatable UUID id (10 times in a row)" do
      ids = (0..9)
        |> Enum.map(fn (_i) -> IdGenerator.generate_uuid() end)
        |> Enum.uniq()
      assert(length(ids) == 10)
      ids |> Enum.each(fn (id) -> assert(id |> String.match?(~r/^[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{12}$/)) end)
    end

  end

end
