# TODO: Make more relevant test for the lack of empty lists than 'assert(@domain_storage.read_domains_all!() == [])';
defmodule Linkstory.Plug.RouterTest do
  # NOTE: Since most of the integration tests modify domain data storage they are synchronized;
  use ExUnit.Case, async: false
  use Plug.Test
  alias Plug.Conn
  alias Linkstory.Router
  alias Linkstory.Util.DateTimeMock
  alias Linkstory.Util.IdGeneratorMock
  alias Linkstory.Data.DomainStorageMock
  import Mox

  # NOTE: For the integration tests purposes only;
  @date_time Application.fetch_env!(:linkstory, :date_time)[:impl]
  @id_generator Application.fetch_env!(:linkstory, :id_generator)[:impl]
  @domain_storage Application.fetch_env!(:linkstory, :domain_storage)[:impl]

  @opts Router.init([])

  # NOTE: All data will be deleted from configured domain data storage!!!
  setup_all do
    if @domain_storage != Linkstory.Data.DomainStorageMock, do: @domain_storage.delete_domains_all!()
    on_exit(fn () -> if @domain_storage != Linkstory.Data.DomainStorageMock, do: @domain_storage.delete_domains_all!() end)
    :ok
  end

  setup :verify_on_exit!

  ######################################################################
  # PING
  ######################################################################

  describe "'GET /ping.text'" do

    @tag :unit_test
    test "(U) returns response with 200 status and valid ping result as text encoded with utf-8" do
      DateTimeMock |> expect(:now_unix, fn () -> "1572548956" end)
      IdGeneratorMock |> expect(:generate_uuid, fn () -> "5ad71aff-a7fb-408d-8cdd-1f9ede6ef484" end)
      conn = conn(:get, "/ping.text")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 200)
      assert(conn |> Conn.get_resp_header("content-type") == ["text/plain; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      assert(conn.resp_body == "pong")
    end

  end

  describe "'GET /ping.json'" do

    @tag :unit_test
    test "(U) returns response with 200 status and valid ping result as json encoded with utf-8" do
      DateTimeMock |> expect(:now_unix, fn () -> "1572843794" end)
      IdGeneratorMock |> expect(:generate_uuid, fn () -> "66d8561c-56d3-4dff-9006-2410342c7982" end)
      conn = conn(:get, "/ping.json")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 200)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      assert(conn.resp_body == ~s/{"ping":"pong"}/)
    end

  end

  ######################################################################
  # INEXISTENT RESOURCES
  ######################################################################

  # TODO: Add post/put/delete/etc tests for inexistent and existent resources;
  describe "'GET /some-random-inexistent-resource'" do

    @tag :unit_test
    test "(U) returns response with 404 status and valid inexistent resource result as json encoded with utf-8" do
      DateTimeMock |> expect(:now_unix, fn () -> "1572985641" end)
      IdGeneratorMock |> expect(:generate_uuid, fn () -> "98a04cce-d2fc-44e7-bcf8-f6cc03b52923" end)
      conn = conn(:get, "/some-random-inexistent-resource")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 404)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      assert(conn.resp_body == ~s/{"status":"not_found","error_ref":"98a04cce-d2fc-44e7-bcf8-f6cc03b52923","error_code":"NFE001"}/)
    end

    @tag :integration_test
    test "(I) returns response with 404 status and valid inexistent resource result as json encoded with utf-8" do
      conn = conn(:get, "/some-random-inexistent-resource")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 404)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      resp_body_pattern =
        ~r/\{"status":"not_found","error_ref":"[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{12}","error_code":"NFE001"\}/
      assert(conn.resp_body |> String.match?(resp_body_pattern))
    end

  end

  ######################################################################
  # (C)RUD
  ######################################################################

  describe "'POST /visited_links'" do

    @tag :unit_test
    test "(U) doesn't accept request with empty body and returns response with 500 status and valid fail result as json encoded with utf-8" do
      DateTimeMock |> expect(:now_unix, fn () -> "1572754129" end)
      IdGeneratorMock |> expect(:generate_uuid, fn () -> "7e1378bf-6696-4151-8228-d119a1b02d22" end)
      conn = conn(:post, "/visited_links")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 500)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      assert(conn.resp_body == ~s/{"status":"internal_server_error","error_ref":"7e1378bf-6696-4151-8228-d119a1b02d22","error_code":"ISE002"}/)
    end

    @tag :integration_test
    test "(I) doesn't accept request with empty body and returns response with 500 status and valid fail result as json encoded with utf-8" do
      @domain_storage.delete_domains_all!()
      assert(@domain_storage.read_domains_all!() == [])
      conn = conn(:post, "/visited_links")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 500)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      resp_body_pattern =
        ~r/\{"status":"internal_server_error","error_ref":"[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{12}","error_code":"ISE002"\}/
      assert(conn.resp_body |> String.match?(resp_body_pattern))
      assert(@domain_storage.read_domains_all!() == [])
    end

    # TODO: Add the same integration test;
    @tag :unit_test
    test "(U) doesn't accepts empty links list but returns response with 200 status and valid success result as json encoded with utf-8" do
      DateTimeMock |> expect(:now_unix, fn () -> "1572549327" end)
      IdGeneratorMock |> expect(:generate_uuid, fn () -> "16255c42-4fad-47a9-ae60-62200fe9d6c0" end)
      DomainStorageMock |> expect(:save_domains!, 0, fn (_domains, _timestamp) -> :ok end)
      conn = conn(:post, "/visited_links", Poison.encode!(%{links: []}))
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 200)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      assert(conn.resp_body == ~s/{"status":"ok"}/)
    end

    @tag :integration_test
    test "(I) doesn't accepts empty links list but returns response with 200 status and valid success result as json encoded with utf-8" do
      @domain_storage.delete_domains_all!()
      assert(@domain_storage.read_domains_all!() == [])
      conn = conn(:post, "/visited_links", Poison.encode!(%{links: []}))
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 200)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      assert(conn.resp_body == ~s/{"status":"ok"}/)
      assert(@domain_storage.read_domains_all!() == [])
    end

    # TODO: Add tests with invalid inbound data format;

    @tag :unit_test
    test "(U) accepts not empty links list and returns response with 200 status and valid success result as json encoded with utf-8" do
      DateTimeMock |> stub(:now_unix, fn () -> "1572845217" end)
      IdGeneratorMock |> stub(:generate_uuid, fn () -> "688a0c23-ad11-49ee-976a-409c97f03292" end)
      # TODO: Add more test cases;
      test_data = [
        {["www.ya.ru", "ya.ru", "ya.ru", "/google.com", "https://www1.я.ру"], ["ya.ru", "я.ру"], 200, "application/json; charset=utf-8", ~s/{"status":"ok"}/}
      ]
      test_data |> Enum.each(fn ({req_links, saved_domains, resp_status, resp_content_type, resp_body}) ->
        DomainStorageMock |> expect(:save_domains!, fn (domains, timestamp) ->
          assert(domains |> Enum.sort() == saved_domains |> Enum.sort())
          assert(timestamp == "1572845217")
          :ok
        end)
        conn = conn(:post, "/visited_links", Poison.encode!(%{links: req_links}))
        conn = Router.call(conn, @opts)
        assert(conn.state == :sent)
        assert(conn.status == resp_status)
        assert(conn |> Conn.get_resp_header("content-type") == [resp_content_type])
        # assert(conn.resp_charset == "utf-8")
        assert(conn.resp_body == resp_body)
      end)
    end

    @tag :integration_test
    test "(I) accepts not empty links list and returns response with 200 status and valid success result as json encoded with utf-8" do
      # TODO: Add more test cases;
      test_data = [
        {["www.ya.ru", "ya.ru", "ya.ru", "/google.com", "https://www1.я.ру"], ["ya.ru", "я.ру"], 200, "application/json; charset=utf-8", ~s/{"status":"ok"}/}
      ]
      test_data |> Enum.each(fn ({req_links, saved_domains, resp_status, resp_content_type, resp_body}) ->
        @domain_storage.delete_domains_all!()
        assert(@domain_storage.read_domains_all!() == [])
        conn = conn(:post, "/visited_links", Poison.encode!(%{links: req_links}))
        conn = Router.call(conn, @opts)
        assert(conn.state == :sent)
        assert(conn.status == resp_status)
        assert(conn |> Conn.get_resp_header("content-type") == [resp_content_type])
        # assert(conn.resp_charset == "utf-8")
        assert(conn.resp_body == resp_body)
        assert(@domain_storage.read_domains_all!() |> Enum.sort() == saved_domains |> Enum.sort())
      end)
    end

  end

  ######################################################################
  # C(R)UD
  ######################################################################

  describe "'GET /visited_domains'" do

    @tag :unit_test
    test "(U) doesn't accept request with empty timestamp range and returns response with 500 status and valid fail result as json encoded with utf-8" do
      DateTimeMock |> expect(:now_unix, fn () -> "1572546783" end)
      IdGeneratorMock |> expect(:generate_uuid, fn () -> "01ed34b0-02a3-408c-913b-bba0b38b75c8" end)
      conn = conn(:get, "/visited_domains")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 500)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      assert(conn.resp_body == ~s/{"status":"internal_server_error","error_ref":"01ed34b0-02a3-408c-913b-bba0b38b75c8","error_code":"ISE003"}/)
    end

    @tag :integration_test
    test "(I) doesn't accept request with empty timestamp range and returns response with 500 status and valid fail result as json encoded with utf-8" do
      @domain_storage.delete_domains_all!()
      assert(@domain_storage.read_domains_all!() == [])
      conn = conn(:get, "/visited_domains")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 500)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      resp_body_pattern =
        ~r/\{"status":"internal_server_error","error_ref":"[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{12}","error_code":"ISE003"\}/
      assert(conn.resp_body |> String.match?(resp_body_pattern))
      assert(@domain_storage.read_domains_all!() == [])
    end

    @tag :unit_test
    test "(U) doesn't accept request with invalid timestamp range and returns response with 500 status and valid fail result as json encoded with utf-8" do
      DateTimeMock |> expect(:now_unix, fn () -> "1572159743" end)
      IdGeneratorMock |> expect(:generate_uuid, fn () -> "59d79d04-5c37-4374-afa1-18c7080a5870" end)
      conn = conn(:get, "/visited_domains?from=12&to=13")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 500)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      assert(conn.resp_body == ~s/{"status":"internal_server_error","error_ref":"59d79d04-5c37-4374-afa1-18c7080a5870","error_code":"ISE003"}/)
    end

    @tag :integration_test
    test "(I) doesn't accept request with invalid timestamp range and returns response with 500 status and valid fail result as json encoded with utf-8" do
      @domain_storage.delete_domains_all!()
      assert(@domain_storage.read_domains_all!() == [])
      conn = conn(:get, "/visited_domains?from=12&to=13")
      conn = Router.call(conn, @opts)
      assert(conn.state == :sent)
      assert(conn.status == 500)
      assert(conn |> Conn.get_resp_header("content-type") == ["application/json; charset=utf-8"])
      # assert(conn.resp_charset == "utf-8")
      resp_body_pattern =
        ~r/\{"status":"internal_server_error","error_ref":"[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{12}","error_code":"ISE003"\}/
      assert(conn.resp_body |> String.match?(resp_body_pattern))
      assert(@domain_storage.read_domains_all!() == [])
    end

    @tag :unit_test
    test "(U) accepts request with valid timestamp range and returns response with 200 status and valid success result as json encoded with utf-8" do
      DateTimeMock |> stub(:now_unix, fn () -> "1572412853" end)
      IdGeneratorMock |> stub(:generate_uuid, fn () -> "79dd34ce-7d91-4f5a-b373-1fa086f8f11a" end)
      test_storage = [
        "1500111111": ["en.wikipedia.org", "ru.wikipedia.org", "youtube.com", "facebook.com"],
        "1500123000": ["ya.ru", "я.ру"],
        "1500123456": ["ya.ru", "я.ру"],
        "1500555555": ["vk.com"],
        "1500999456": ["google.com", "en.wikipedia.org", "habr.com"],
        "1500999555": ["google.com", "фанбокс.ру", "funbox.ru"],
        "1500999999": ["google.com", "mail.google.com"]
      ] |> Enum.sort()
      test_data = [
        # All domains;
        {"1500000000", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","funbox.ru","google.com","habr.com",/
          <> ~s/"mail.google.com","ru.wikipedia.org","vk.com","ya.ru","youtube.com","фанбокс.ру","я.ру"]}/},
        # Domains laing around lowest timestamp (1500111111);
        {"1000000000", "1500000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[]}/},
        {"1000000000", "1500111110", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[]}/},
        {"1000000000", "1500111111", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","ru.wikipedia.org","youtube.com"]}/},
        {"1500111111", "1500111111", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","ru.wikipedia.org","youtube.com"]}/},
        {"1500111111", "1500111112", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","ru.wikipedia.org","youtube.com"]}/},
        # Domains laing around biggest timestamp (1500999999);
        {"1500999998", "1500999999", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"google.com","mail.google.com"]}/},
        {"1500999999", "1500999999", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"google.com","mail.google.com"]}/},
        {"1500999999", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"google.com","mail.google.com"]}/},
        {"1501000000", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[]}/},
        {"1510000000", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[]}/},
        # Domains laing around "random" timestamps;
        {"1500000000", "1500333333", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","ru.wikipedia.org","ya.ru","youtube.com","я.ру"]}/},
        {"1500123456", "1500999556", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","funbox.ru","google.com","habr.com","vk.com","ya.ru","фанбокс.ру","я.ру"]}/},
        {"1500999500", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"funbox.ru","google.com","mail.google.com","фанбокс.ру"]}/}
      ]
      test_data |> Enum.each(fn ({req_from, req_to, resp_status, resp_content_type, resp_body}) ->
        DomainStorageMock |> expect(:read_domains!, fn (from, to) ->
          # NOTE: Make sure that timestamp range boundaries weren't changed by router;
          assert(req_from == from)
          assert(req_to == to)
          test_storage
          |> Enum.filter(fn ({timestamp, domains}) -> to_string(timestamp) >= from and to_string(timestamp) <= to end)
          |> Enum.reduce([], fn ({timestamp, domains}, acc) -> domains ++ acc end)
        end)
        conn = conn(:get, "/visited_domains?from=#{req_from}&to=#{req_to}")
        conn = Router.call(conn, @opts)
        assert(conn.state == :sent)
        assert(conn.status == resp_status)
        assert(conn |> Conn.get_resp_header("content-type") == [resp_content_type])
        # assert(conn.resp_charset == "utf-8")
        assert(conn.resp_body == resp_body)
      end)
    end

    @tag :integration_test
    test "(I) accepts request with valid timestamp range and returns response with 200 status and valid success result as json encoded with utf-8" do
      @domain_storage.delete_domains_all!()
      assert(@domain_storage.read_domains_all!() == [])
      @domain_storage.save_domains!(["en.wikipedia.org", "ru.wikipedia.org", "youtube.com", "facebook.com"], "1500111111")
      @domain_storage.save_domains!(["ya.ru", "я.ру"], "1500123000")
      @domain_storage.save_domains!(["ya.ru", "я.ру"], "1500123456")
      @domain_storage.save_domains!(["vk.com"], "1500555555")
      @domain_storage.save_domains!(["google.com", "en.wikipedia.org", "habr.com"], "1500999456")
      @domain_storage.save_domains!(["google.com", "фанбокс.ру", "funbox.ru"], "1500999555")
      @domain_storage.save_domains!(["google.com", "mail.google.com"], "1500999999")
      assert(@domain_storage.read_domains_all!() |> Enum.sort() == [
        "mail.google.com", "funbox.ru", "ya.ru", "я.ру", "facebook.com", "фанбокс.ру",
        "google.com", "habr.com", "vk.com", "en.wikipedia.org", "ru.wikipedia.org", "youtube.com"] |> Enum.sort())
      test_data = [
        # All domains;
        {"1500000000", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","funbox.ru","google.com","habr.com",/
          <> ~s/"mail.google.com","ru.wikipedia.org","vk.com","ya.ru","youtube.com","фанбокс.ру","я.ру"]}/},
        # Domains laing around lowest timestamp (1500111111);
        {"1000000000", "1500000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[]}/},
        {"1000000000", "1500111110", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[]}/},
        {"1000000000", "1500111111", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","ru.wikipedia.org","youtube.com"]}/},
        {"1500111111", "1500111111", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","ru.wikipedia.org","youtube.com"]}/},
        {"1500111111", "1500111112", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","ru.wikipedia.org","youtube.com"]}/},
        # Domains laing around biggest timestamp (1500999999);
        {"1500999998", "1500999999", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"google.com","mail.google.com"]}/},
        {"1500999999", "1500999999", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"google.com","mail.google.com"]}/},
        {"1500999999", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"google.com","mail.google.com"]}/},
        {"1501000000", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[]}/},
        {"1510000000", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[]}/},
        # Domains laing around "random" timestamps;
        {"1500000000", "1500333333", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","facebook.com","ru.wikipedia.org","ya.ru","youtube.com","я.ру"]}/},
        {"1500123456", "1500999556", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"en.wikipedia.org","funbox.ru","google.com","habr.com","vk.com","ya.ru","фанбокс.ру","я.ру"]}/},
        {"1500999500", "2000000000", 200, "application/json; charset=utf-8", ~s/{"status":"ok","domains":[/
          <> ~s/"funbox.ru","google.com","mail.google.com","фанбокс.ру"]}/}
      ]
      test_data |> Enum.each(fn ({req_from, req_to, resp_status, resp_content_type, resp_body}) ->
        conn = conn(:get, "/visited_domains?from=#{req_from}&to=#{req_to}")
        conn = Router.call(conn, @opts)
        assert(conn.state == :sent)
        assert(conn.status == resp_status)
        assert(conn |> Conn.get_resp_header("content-type") == [resp_content_type])
        # assert(conn.resp_charset == "utf-8")
        assert(conn.resp_body == resp_body)
      end)
    end

  end

end
