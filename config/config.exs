import Config

case Mix.env() do
  # NOTE: Default mix test environment is disabled because of "test.integration" allias unexpected behaviuor;
  :test_unit ->
    config(:linkstory, :date_time, [impl: Linkstory.Util.DateTimeMock])
    config(:linkstory, :id_generator, [impl: Linkstory.Util.IdGeneratorMock])
    config(:linkstory, :domain_storage, [impl: Linkstory.Data.DomainStorageMock])
  :test_integration ->
    config(:linkstory, :date_time, [impl: Linkstory.Util.DateTime])
    config(:linkstory, :id_generator, [impl: Linkstory.Util.IdGenerator])
    config(:linkstory, :domain_storage, [impl: Linkstory.Data.DomainStorageRedis, host: "localhost", port: 6379])
  :dev ->
    config(:linkstory, :server, [port: 8085])
    config(:linkstory, :date_time, [impl: Linkstory.Util.DateTime])
    config(:linkstory, :id_generator, [impl: Linkstory.Util.IdGenerator])
    config(:linkstory, :domain_storage, [impl: Linkstory.Data.DomainStorageRedis, host: "localhost", port: 6379])
  :prod ->
    config(:linkstory, :server, [port: 9095])
    config(:linkstory, :date_time, [impl: Linkstory.Util.DateTime])
    config(:linkstory, :id_generator, [impl: Linkstory.Util.IdGenerator])
    config(:linkstory, :domain_storage, [impl: Linkstory.Data.DomainStorageRedis, host: "localhost", port: 6379])
  any ->
    exit({:config, :unexpected_mix_environment, any})
end
